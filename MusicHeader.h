#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <windows.h>
#pragma comment(lib,"winmm.lib")
using namespace std;


class List
{
	private:
		typedef struct node
		{
			int data;
			int dataPlaylist;
			int dataQueue;
			int dataGenre;
			string musicTitle;
			string musicArtist;
			string musicGenre;
			string musicAlbum;
			bool inPlaylist;
			bool inQueue;
			
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;

	public:
		List();
		void insertNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum,int dataGenre);
		void Library();
		int Add_PlayList(int addData, int counter);
		int Remove_Playlist(int counter);
		void PlayList();
		int Enqueue(int qData,int counter);
		int Dequeue();
		void sortByAlbum();
		void viewQueue(int queueCounter);
		void playQueue(int queueCounter);
		void playStack(int stackCounter);
		void RandomPlaylist(int random);
};

//INSTANTIATING POINTERS//
List::List()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}
//PUSH NODE / CREATE NODE//
void List::insertNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum, int dataGenre)
{
	nodePtr n = new node;
	n->next = NULL;
	if(head!=NULL)
	{
		curr = head;
		temp = head;
		while(curr->next != NULL)
		{
			curr = curr->next;
			temp = curr;
		}
		curr->next =n;
		curr = curr->next;
		curr->data = temp->data+1;
		//PUT COMPONENTS OF NODE HERE
		system("CLS");
		//////////////////
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, 78); 
		cout 
		<<"" << endl
		<<"" << endl
		<<"  $     $  $    $ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl
		<<"  $$   $$  $    $ $         $   $             #  #    # ##    #  #       " << endl
		<<"  $ $ $ $  $    $ $         $   $            #   #    # # #   #  #       " << endl
		<<"  $  $  $  $    $ $$$$$$    $   $           #    #    # #  #  #  ####    " << endl
		<<"  $     $  $    $      $    $   $          #     #    # #   # #  #       " << endl
		<<"  $     $  $    $      $    $   $         #      #    # #    ##  #       " << endl
		<<"  $     $  $$$$$$ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl		
		<<"" << endl
		<<"" << endl << endl;
		SetConsoleTextAttribute(hConsole, 11); 
	int i, c, d;
	char a = 177, b=219;	
	cout << "\n\t\t\t       ADDING MUSIC" << endl;
	cout << "\t\t\tLOADING......." << endl;
	cout << "\t\t\t";
	for (i = 0; i <25; i++)
	cout << a;
	c = rand()%1;
	Sleep(c);
	cout << "\r";
	cout << "\t\t\t";
	for (i = 0; i <25; i++){
	cout << b;
	d = rand()%1;
	Sleep(d);
	}
	}
	else
	{
		head = n;
		n->data =0;
	}
	top = n;
	n->musicTitle = musicTitle;
	n->musicArtist = musicArtist;
	n->musicGenre = musicGenre;
	n->musicAlbum = musicAlbum;
	n->inPlaylist = false;
	n->inQueue = false;
	n->dataPlaylist =0;
	n->dataQueue =0;
	n->dataGenre = dataGenre;
}

//DISPLAY STACK//
void List::Library()
{
//	cout << "COUNTER: "<<top->data <<endl;
	cout << " MUSIC LIBRARY " << endl;
	for(int i = 1;i<=top->data;i++)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->data!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0)
		{
		}
		else
		{
			cout << curr->data << ") " << curr->musicTitle << " // " << curr->musicAlbum << " // " <<curr->musicGenre << endl;
		}
	} 
	cout << "" << endl;
}

//VIEWING PLAYLIST//
void List::PlayList()
{
	cout << " PLAY LIST " << endl;
	for(int i = top->data;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->dataPlaylist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
		{
		}
		else
		{
			cout << curr->dataPlaylist << ") " << curr->musicTitle << " // "<< curr->musicAlbum <<" // "<<curr->musicGenre << endl;
		}
	}
	cout << "" << endl;
}

//VIEWING QUEUE//
void List::viewQueue(int queueCounter)
{
	cout << " QUEUE " << endl;
	if(queueCounter==1)
	{
		cout << "      No Music in Queue." <<endl;
	}
	else
	{
		for(int i=0;i<=top->data;i++)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->dataQueue!=i)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
			{
			}
			else
			{
				cout << curr->dataQueue << ") " << curr->musicTitle << "//" << curr->musicAlbum << "//" <<curr->musicGenre << endl;
			}
		}	
	}
	cout << "" << endl;
}

//ADDING TO PLAYLIST//
int List::Add_PlayList(int addData, int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != addData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << addData << " Error. Music not found." << endl;
		return 0;
	}
	else if(curr->inPlaylist==true)
	{
		return 0;
	}
	else
	{	
		curr->dataPlaylist = counter;
		curr->inPlaylist =true;
		return 1; 
	}
}

//REMOVING FROM PLAYLIST//
int List::Remove_Playlist(int counter)
{
	int delData = counter -1;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> dataPlaylist != delData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		return 0;
	}
	else if(curr->inPlaylist==false)
	{
		return 0;
	}
	else
	{	
		cout << "Music Removed" << endl;
		curr->dataPlaylist = 0;
		curr->inPlaylist =false;
		return 1; 
	}
}

//ENQUEUE / ADD TO QUEUE
int List::Enqueue(int qData,int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != qData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << qData << " Error. Music not found." << endl;
		return 0;
	}
	else if(curr->inQueue==true)
	{
		return 0;
	}
	else
	{	
		curr->dataQueue = counter;
		curr->inQueue =true;
		return 1; 
	}
}

//DEQUEUE / REMOVE FROM QUEUE
int List::Dequeue()
{
	curr=head;
	while(curr->next!=NULL)
	{
		if(curr->dataQueue!=0)
		{
			curr->dataQueue=curr->dataQueue-1;
			if(curr->dataQueue==0)
			{
				curr->inQueue=false;			
			}
		}
		curr=curr->next;
	}
	return 1;
}

//PLAY PLAYLIST
void List::playStack(int stackCounter)
{
	int i=1;
	while(i<=stackCounter)
	{
		if(stackCounter==1)
		{
			break;
		}
		system("CLS");
//		cout << " PLAY LIST  " << endl;
		for(int x = top->data;x>=0;x--)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->dataPlaylist!=x)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
			{
			}
			else
			{
//				cout << curr->dataPlaylist << ") " << curr->musicTitle << " // " << curr->musicAlbum << " // " <<curr->musicGenre << endl;
			}
		}
		curr=head;
		while(i-1!=curr->dataPlaylist)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
	cout
				<< "      " << endl
				<< "                           -- Playlist --                       " << endl 
				<< "                                                                " << endl
				<< "            1 : Next Music                                      " << endl
				<< "            2 : Back to Menu                                    " << endl
				<< "                                                                " << endl 
				<< "                                                                " << endl 						
				<< "      " << endl 
			 
			 <<"      Now Playing: "<< curr->musicTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION FUCKING SHIT	
		cout << "Enter the number of your choice: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}
}

//PLAY QUEUE
void List::playQueue(int queueCounter)
{
	int i=1;
	while(i<=queueCounter)
	{
		if(queueCounter==1)
		{
			break;
		}
		system("CLS");
		cout << " QUEUE " << endl;
		if(queueCounter==1)
		{
			cout << "      No Queue." <<endl;
		}
		else
		{
			for(int x=0;x<=top->data;x++)
			{
				curr=head;
				temp=head;
				while(curr!=NULL&&curr->dataQueue!=x)
				{
					curr = curr->next;
				}
				if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
				{
				}
				else
				{
					cout << curr->dataQueue << ") " << curr->musicTitle << " // " << curr->musicAlbum << " // " <<curr->musicGenre << endl;
				}
			}	
		}
		cout << "" << endl;
		
		curr=head;
		while(i-1!=curr->dataQueue)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout
				<< "      " << endl
				<< "                             -- Queue --                        " << endl 
				<< "                                                                " << endl
				<< "            1 : Next Music                                      " << endl
				<< "            2 : Back to Menu                                    " << endl
				<< "                                                                " << endl 
				<< "                                                                " << endl 						
				<< "      " << endl 
			 
			 <<"      Now Playing: "<< curr->musicTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION FUCKING SHIT	
		cout << "Enter the number of your choice: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}	
}
// SELECTION SORTING
void List::sortByAlbum()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int i = 2;
	for(i;i<=12;i++)
	{
		curr=head;
		temp=head;
		while(curr->next!=NULL)//Scan the whole linked list
		{
			if(curr->dataGenre == i)//try to find each "i", dataGenre is key int of Each Genre, ex: HIPHOP = 1, POP PUNK = 3.
			// This loop will only find each key and print it, and then loop again until all are sorted according to GENRE
			{
				SetConsoleTextAttribute(hConsole, i);  
				cout << curr->musicGenre <<"  " << curr->musicTitle << " // " << curr->musicAlbum << " // " <<endl;
			}
			curr=curr->next;
		}
	}
}

//RANDOM PLAYLIST
void List::RandomPlaylist(int random)
{
	int i=1;
	while(i<=13)
	{
		int x=1;
		while(curr->next!=NULL&&x<=10)
		{
			if(curr->dataGenre==random)
			{
				cout << curr->musicGenre <<"  " << curr->musicTitle << " // " << curr->musicAlbum << " // " <<endl;
				x=x+1;
			}
			curr=curr->next;
		}
	}
}


