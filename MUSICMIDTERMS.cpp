/* Create a C++ program implementing linked lists to store a list of music titles. You must be able to perform the following operations:
- insert a music title
- view a music title, the title previously viewed and the next on queue
- edit a music title
- delete a music title
- view the list of all music titles stored

Sample input: "Buwan by JKL"

Sample output:
Playing - "Everything by Never The Strangers" 
Previous - "Bawat daan by Ebe Dancel"
Next - "Dating Tayo by TJ Monterde" */
	
#include <iostream>
#include <windows.h>
#include <string.h>
#include <string>

using namespace std;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
int choice;
class Node{
	public:
		string data;
		Node * next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			void DisplayList(void);
			void DisplayPlaylist(void);
			string ViewMusic(int index);
	private:
			Node* head;
};
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
void List::DisplayPlaylist(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::InsertNode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
void StoreDesign(){
		SetConsoleTextAttribute(hConsole, 3); 
		cout 
		<<"฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿" << endl
		<<"อออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออ" << endl
		<<"  $     $  $    $ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl
		<<"  $$   $$  $    $ $         $   $             #  #    # ##    #  #       " << endl
		<<"  $ $ $ $  $    $ $         $   $            #   #    # # #   #  #       " << endl
		<<"  $  $  $  $    $ $$$$$$    $   $           #    #    # #  #  #  ####    " << endl
		<<"  $     $  $    $      $    $   $          #     #    # #   # #  #       " << endl
		<<"  $     $  $    $      $    $   $         #      #    # #    ##  #       " << endl
		<<"  $     $  $$$$$$ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl		
		<<"อออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออออ" << endl
		<<"฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿" << endl << endl << endl;
		system("pause");
		system("cls");
	}
void Menu(){
	SetConsoleTextAttribute(hConsole, 6);
	cout
			<< "      ฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿" << endl
			<< "      ฿                       Main Menu                          ฿" << endl 
			<< "      ฿      1 : Insert Music Title                              ฿" << endl
			<< "      ฿      2 : View Music Title                                ฿" << endl
			<< "      ฿      3 : Edit Music Title                                ฿" << endl 
			<< "      ฿      4 : Delete Music Title                              ฿" << endl 
			<< "      ฿      5 : View the List of all Music Titles               ฿" << endl 
			<< "      ฿      6 : Exit                                            ฿" << endl
			<< "      ฿                                                          ฿" << endl  
			<< "      ฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿฿" << endl 
			<< "              Enter the number of your chosen option: ";	
			cin >> choice;
}		
int main()
{
	StoreDesign();
		List musicList;
		string musicTitle, editTitle;
		int musicNum;
		musicList.InsertNode(0,"Tensionado by Soapdish");
		musicList.InsertNode(1,"Ehu Girl by Kolohe Kai");
		musicList.InsertNode(2,"Jopay by Mayonnaisse");
		musicList.InsertNode(3,"Batang Pasaway by Psychedelic Boyz");
		musicList.InsertNode(4,"Leaves by Ben&Ben");
		again:
	Menu();
	switch(choice)
	{
		case 1: //Insert
			system("cls");
			cout << " อออออออออออออออออ INSERT MUSIC TITLE อออออออออออออออออ" << endl;
			cout << "Example input: Buwan by Juan Karlos Labajo"<<endl;
			cout << "Input the music title you want to Insert: ";
			cin.ignore();
			getline(cin, musicTitle);
			musicList.InsertNode(0,musicTitle);
			musicList.DisplayList();
			system("pause");
			system("cls");
			goto again;
		case 2://View
			system("cls");
			cout << " อออออออออออออออออ VIEW MUSIC TITLE อออออออออออออออออ" << endl;
			cout<<"Enter the number of the music title you want to view: " << endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			if(musicNum <= 1)
				{
				cout<<"Previous: No Song" <<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			else
				{
				cout<<"Previous: "<<musicList.ViewMusic(musicNum-1)<<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			system("pause");
			system("cls");
			goto again;
		case 3://Edit
			system("cls");
			cout << " อออออออออออออออออ EDIT MUSIC TITLE อออออออออออออออออ" << endl;
			cout<<"Enter the number of the music title you want to edit: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			cout<<"Enter the new music title: ";
			cin.ignore();
			getline(cin, editTitle);
			cout<<endl;
			musicList.InsertNode(musicNum-1,editTitle);
			system("pause");
			system("cls");
			goto again;
		case 4://Delete
			system("cls");
			cout << " อออออออออออออออออ DELETE MUSIC TITLE อออออออออออออออออ" << endl;		
			cout<<"Enter the number of the music title you want to delete: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			system("pause");
			system("cls");
			goto again;
		case 5://View all
			system("cls");
			cout << " อออออออออออออออออ MUSIC LIST อออออออออออออออออ" << endl;
			musicList.DisplayPlaylist();
			system("pause");
			system("cls");
			goto again;
		case 6://Exit
			system("cls");
			main();
		default:
			cout<<""<<endl;
			cout<<"     Invalid input! Please try again."<<endl;
			system("pause");
			system("cls");
			goto again;
	}	
}
	
