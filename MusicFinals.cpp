#include <ctime>
#include "MusicHeader.h"

void Design(){
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, 78); 
		cout 
		<<"" << endl
		<<"" << endl
		<<"  $     $  $    $ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl
		<<"  $$   $$  $    $ $         $   $             #  #    # ##    #  #       " << endl
		<<"  $ $ $ $  $    $ $         $   $            #   #    # # #   #  #       " << endl
		<<"  $  $  $  $    $ $$$$$$    $   $           #    #    # #  #  #  ####    " << endl
		<<"  $     $  $    $      $    $   $          #     #    # #   # #  #       " << endl
		<<"  $     $  $    $      $    $   $         #      #    # #    ##  #       " << endl
		<<"  $     $  $$$$$$ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl		
		<<"" << endl
		<<"" << endl << endl;
	}
	
int main () 
{
//// SETTING CONSOLE COLOR
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	List Music;
	int playListMusic=1;
	int QueueMusic=1;
	//START
	Music.insertNode("","","","",0);
	//PERIPHERAL VISION 10
	Music.insertNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	//FOREST HILLS DRIVE 2014 33
	Music.insertNode("Intro - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("Hello - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	Music.insertNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2034 Forrest Hills Drive",3);
	// WE ARE NOT YOUR KIND 14
	Music.insertNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);	
	Music.insertNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	//Enema of the State 12
	Music.insertNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	//American FootBall 9
	Music.insertNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	//LNOTGY 10
	Music.insertNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Smooth Seas Dont Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	//Doo-Wops & Hooligans 11
	Music.insertNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	//Cutterpillow  13
	Music.insertNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	//  Save Rock & Roll
	Music.insertNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	// Clarity
	Music.insertNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Shave it - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Clarity - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Codec - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Stache - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Epos - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Push Play - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Alive - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",7);
	//NOthing Personal
	Music.insertNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	//All We Know is Falling
	Music.insertNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	//If You Were a Movie, This Would Be Your Soundtrack
	Music.insertNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	//The Finer Things (Acoustic)
	Music.insertNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	//Science & Faith
	Music.insertNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	//Move Along
	Music.insertNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	//Sweetener
	Music.insertNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	//Red
	Music.insertNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",7);
	//Hopeless Fountain Kingdom
	Music.insertNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	//Honey Works
	Music.insertNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	//Mind Of Mine
	Music.insertNode("PILLOWTALK - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?iTs YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BeFoUr - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("sHe - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("dRuNk - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?rEaR vIeW - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?wRoNg - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?fOoL fOr YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BoRdErZ - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?tRuTh - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("lUcOzAdE - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("TiO - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BLUE - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BRIGHT - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("LIKE I WOULD - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("SHE DONT LOVE ME - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	//FOUR
	Music.insertNode("Steal My Girl - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Ready to Run - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Where Do Broken Hearts Go - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("18 - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Girl Almighty - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Fools Gold - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Night Changes - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("No Control - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Fireproof - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Spaces - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Stockholm Syndrome - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Clouds - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Change Your Ticket - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Illusion - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Once in a Lifetime - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Act My Age - One Direction", "One Direction", "POP", "FOUR",7);
	//Multiply
	Music.insertNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	//Divide
	Music.insertNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	//1989
	Music.insertNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	//After Laughter
	Music.insertNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	//Need You Now
	Music.insertNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	//HeartBreak
	Music.insertNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	//Symphony Soldier
	Music.insertNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	//Freudian
	Music.insertNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	//Case Study 01
	Music.insertNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	// Natty Dread
	Music.insertNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Rebel Music - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	//Legend
	Music.insertNode("Is This Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	//Rastaman Vibrations
	Music.insertNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	//4 your Eyez only
	Music.insertNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("She's Mine Pt. 3 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	//Russ The Mixtape
	Music.insertNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	//Kupal Destroyerr 1
	Music.insertNode("Lamasin Ang Osus- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Animal - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Coming - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Makapal ang Blbl mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sir tngna mo pkyu ALbum VEr - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Isubo ang ulo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Ptngna 2 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("PtngnaTrapik ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sira Ulo ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Ptngna 1 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Anak ka ng Pakyu Album Ver - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Kupal Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Galit ako sa Gwapo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Mamatay ka na - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Mamatay ka na sana - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Gard Tngna mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("BAstos Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);	
	Music.insertNode("Mamatay ka na sana Album VEr- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sir Tanginamo Pakyuuu - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	//December Avenue (2017)
	Music.insertNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Fallin - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Ill Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Dive - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Forever - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Breathe Again - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Time To Go - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	//Too Weird To Live, Too Rare To Die!
	Music.insertNode("Casual Affair - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Collar Full  - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Girl That You Love - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Miss Jackson - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Nicotine - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("The End Of All Things - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("This Is Gospel - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Vegas Lights - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	//Master Of Puppets
	Music.insertNode("Battery - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Damage, Inc. - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Disposable Heroes - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Leper Messiah - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Master Of Puppets - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Orion - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("The Thing That Should Be - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Welcome Home (Sanitarium) - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	//Get Your Heart On!
	Music.insertNode("You Suck at Love - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Can't Keep My Hands off You - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Jet Lag - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Astronaut - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Loser of the Year - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Anywhere Else But Here - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Freaking Me Out - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Summer Paradise - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Gone Too Soon - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Last One Standing - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("This Song Saved My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	//Still Not Getting Any
	Music.insertNode("Shut Up! - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Welcome to My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Perfect World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Thank You - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Me Against the World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Jump - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Everytime - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Promise - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("One - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Untitled - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Perfect - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	//Vessels
	Music.insertNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("The Run and Go - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Fake You Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Guns for Hands - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Trees - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Truce - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	//Blurry Face
	Music.insertNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);
	Music.insertNode("Goner - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",12);

	Music.insertNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	Music.insertNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",6);
	//FOREST HILLS DRIVE 2014 13
	Music.insertNode("Intro - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("Hello - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	Music.insertNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",3);
	// WE ARE NOT YOUR KIND 14
	Music.insertNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);	
	Music.insertNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	Music.insertNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",8);
	//Enema of the State 12
	Music.insertNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	Music.insertNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",5);
	//American FootBall 9
	Music.insertNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	Music.insertNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",12);
	//LNOTGY 10
	Music.insertNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Smooth Seas Dont Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	Music.insertNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",5);
	//Doo-Wops & Hooligans 11
	Music.insertNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	Music.insertNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",7);
	//Cutterpillow  13
	Music.insertNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	Music.insertNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",7);
	//  Save Rock & Roll
	Music.insertNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	Music.insertNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",9);
	// Clarity
	Music.insertNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Shave it - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Clarity - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Codec - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Stache - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Epos - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Push Play - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Alive - Zedd", "Zedd", "POP", "Clarity",7);
	Music.insertNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",7);
	//NOthing Personal
	Music.insertNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	Music.insertNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",5);
	//All We Know is Falling
	Music.insertNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	Music.insertNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",9);
	//If You Were a Movie, This Would Be Your Soundtrack
	Music.insertNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	Music.insertNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",10);
	//The Finer Things (Acoustic)
	Music.insertNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	Music.insertNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",10);
	//Science & Faith
	Music.insertNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	Music.insertNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",6);
	//Move Along
	Music.insertNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	Music.insertNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",5);
	//Sweetener
	Music.insertNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	Music.insertNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",7);
	//Red
	Music.insertNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",7);
	Music.insertNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",7);
	//Hopeless Fountain Kingdom
	Music.insertNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	Music.insertNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",13);
	//Honey Works
	Music.insertNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",1);
	Music.insertNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",1);
	//Mind Of Mine
	Music.insertNode("PILLOWTALK - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?iTs YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BeFoUr - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("sHe - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("dRuNk - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?rEaR vIeW - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?wRoNg - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?fOoL fOr YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BoRdErZ - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("?tRuTh - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("lUcOzAdE - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("TiO - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BLUE - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("BRIGHT - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("LIKE I WOULD - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	Music.insertNode("SHE DONT LOVE ME - ZAYN", "ZAYN", "POP", "Mind of Mine",7);
	//FOUR
	Music.insertNode("Steal My Girl - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Ready to Run - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Where Do Broken Hearts Go - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("18 - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Girl Almighty - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Fools Gold - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Night Changes - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("No Control - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Fireproof - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Spaces - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Stockholm Syndrome - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Clouds - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Change Your Ticket - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Illusion - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Once in a Lifetime - One Direction", "One Direction", "POP", "FOUR",7);
	Music.insertNode("Act My Age - One Direction", "One Direction", "POP", "FOUR",7);
	//Multiply
	Music.insertNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	Music.insertNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",7);
	//Divide
	Music.insertNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	Music.insertNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",7);
	//1989
	Music.insertNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	Music.insertNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",7);
	//After Laughter
	Music.insertNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	Music.insertNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",6);
	//Need You Now
	Music.insertNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	Music.insertNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",2);
	//HeartBreak
	Music.insertNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	Music.insertNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",2);
	//Symphony Soldier
	Music.insertNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	Music.insertNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",6);
	//Freudian
	Music.insertNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	Music.insertNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",4);
	//Case Study 01
	Music.insertNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	Music.insertNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",4);
	// Natty Dread
	Music.insertNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Rebel Music - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	Music.insertNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",11);
	//Legend
	Music.insertNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	Music.insertNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",11);
	//Rastaman Vibrations
	Music.insertNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	Music.insertNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",11);
	//4 your Eyez only
	Music.insertNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("She's Mine Pt. 3 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	Music.insertNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",3);
	//Russ The Mixtape
	Music.insertNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	Music.insertNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",3);
	//Kupal Destroyerr 1
	Music.insertNode("Lamasin Ang Osus- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Animal - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Coming - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Makapal ang Blbl mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sir tngna mo pkyu ALbum VEr - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Isubo ang ulo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Ptngna 2 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("PtngnaTrapik ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sira Ulo ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Ptngna 1 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Anak ka ng Pakyu Album Ver - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Kupal Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Galit ako sa Gwapo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Mamatay ka na - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Mamatay ka na sana - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Gard Tngna mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("BAstos Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);	
	Music.insertNode("Mamatay ka na sana Album VEr- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	Music.insertNode("Sir Tanginamo Pakyuuu - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",8);
	//December Avenue (2017)
	Music.insertNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Fallin - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Ill Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Dive - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Forever - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Breathe Again - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	Music.insertNode("Time To Go - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",6);
	//Too Weird To Live, Too Rare To Die!
	Music.insertNode("Casual Affair - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Collar Full  - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Girl That You Love - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Miss Jackson - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Nicotine - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("The End Of All Things - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("This Is Gospel - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	Music.insertNode("Vegas Lights - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",9);
	//Master Of Puppets
	Music.insertNode("Battery - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Damage, Inc. - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Disposable Heroes - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Leper Messiah - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Master Of Puppets - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Orion - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("The Thing That Should Be - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	Music.insertNode("Welcome Home (Sanitarium) - METALLICA", "METALLICA", "METAL", "Master Of Puppets",8);
	//Get Your Heart On!
	Music.insertNode("You Suck at Love - Simple Plan", "Simple Plan", "POP-PUNK", "Get2 Your Heart On!",5);
	Music.insertNode("Can't Keep My Hands off You - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Jet Lag - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Astronaut - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Loser of the Year - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Anywhere Else But Here - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Freaking Me Out - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Summer Paradise - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Gone Too Soon - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("Last One Standing - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	Music.insertNode("This Song Saved My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",5);
	//Still Not Getting Any
	Music.insertNode("Shut Up! - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Welcome to My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Perfect World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Thank You - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Me Against the World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Jump - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Everytime - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Promise - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("One - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Untitled - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	Music.insertNode("Perfect - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",5);
	//Vessels
	Music.insertNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);
	Music.insertNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",12);


//START OF THE PROGRAM
	int random;
	srand( time(0));
	random=rand()%10+1;
	bool systemLoopCondition = true;
	while(systemLoopCondition == true)
	{
		cin.clear();
		system("CLS");
		Design();
		int choice;
		SetConsoleTextAttribute(hConsole, 10);
		cout 
		<< "" << endl;
//			cout << "    Shows a random number between 1-10: " << random <<endl;
			cout << "    Number of Music in Playlist: "<< playListMusic-1 <<endl
				 << "    Number of Music in Queue: "<< QueueMusic-1 <<endl
		<<	"" << endl;
		SetConsoleTextAttribute(hConsole, 14);
		cout
				<< "      " << endl
				<< "                          -- Main Menu --                       " << endl 
				<< "                                                                " << endl
				<< "            1 : Music Library                                   " << endl
				<< "            2 : Playlist                                        " << endl
				<< "            3 : Queue                                           " << endl 
				<< "            4 : Sort                                            " << endl 
				<< "            5 : Exit                                            " << endl 	
				<< "                                                                " << endl 
				<< "                                                                " << endl 						
				<< "      " << endl 
				<< "              Enter the number of your chosen option: "  ;
			cin >> choice;
//////////ERROR CATCHER
		if(cin.fail())
		{
			cin.clear();
			cin.ignore();
		}
//////////CHOICE 1
		else if (choice == 1)
		{
			system("CLS");
			SetConsoleTextAttribute(hConsole, 10);
			Music.Library();
			system("PAUSE");
		}
//////////CHOICE 2
		else if (choice ==2)
		{	
			system("CLS");
			SetConsoleTextAttribute(hConsole, 10);  
			Music.PlayList(); 
			int playlistChoice;
			int muschoice;
			SetConsoleTextAttribute(hConsole, 14);
			cout
				<< "      " << endl
				<< "                           -- Playlist --                       " << endl 
				<< "                                                                " << endl
				<< "            1 : Add to Playlist                                 " << endl
				<< "            2 : Remove from Playlist                            " << endl
				<< "            3 : Play Music                                      " << endl 
				<< "            4 : Back to Menu                                    " << endl 
				<< "                                                                " << endl 
				<< "                                                                " << endl 						
				<< "      " << endl 
				<< "              Enter the number of your chosen option: "  ;
			cin >> playlistChoice;
			if(cin.fail()||playlistChoice>3||playlistChoice<1)
			{
				SetConsoleTextAttribute(hConsole, 10);  
				cout << "Error. Please enter a valid input.";
				system("PAUSE");
			}
			else if(playlistChoice==1)
			{
				SetConsoleTextAttribute(hConsole, 10);  
				Music.Library();
				SetConsoleTextAttribute(hConsole, 14);  
				cout << "Enter the number of the music title you want to add: ";
				cin >> muschoice;
				playListMusic = Music.Add_PlayList(muschoice,playListMusic)+playListMusic;
				system("PAUSE");
			}
			else if(playlistChoice==2)
			{
				playListMusic =playListMusic-Music.Remove_Playlist(playListMusic);
			}
			else if(playlistChoice==3)
			{
				Music.playStack(playListMusic);
			}
			else if(playlistChoice==4)
			{
			}
			else
			{
			}
		}
//////////CHOICE 3
		else if (choice==3)
		{
			system("CLS");
			SetConsoleTextAttribute(hConsole, 10);  
			Music.viewQueue(QueueMusic);
			int playlistChoice;
			int muschoice;
			SetConsoleTextAttribute(hConsole, 14);
			cout
				<< "      " << endl
				<< "                             -- Queue --                        " << endl 
				<< "                                                                " << endl
				<< "            1 : Add to Queue                                    " << endl
				<< "            2 : Remove from Queue                               " << endl
				<< "            3 : Play Music                                      " << endl 
				<< "            4 : Back to Menu                                    " << endl 
				<< "                                                                " << endl 
				<< "                                                                " << endl 						
				<< "      " << endl 
				<< "              Enter the number of your chosen option: "  ;
			cin >> playlistChoice;
			if(cin.fail()||playlistChoice>3||playlistChoice<1)
			{
				SetConsoleTextAttribute(hConsole, 10);  
				cout << "Error. Please enter a valid input.";
				system("PAUSE");
			}
			else if(playlistChoice==1)
			{
				SetConsoleTextAttribute(hConsole, 10);  
				Music.Library();
				SetConsoleTextAttribute(hConsole, 14);  
				cout << "Enter the number of the music title you want to add: ";
				cin >> muschoice;
				QueueMusic = Music.Enqueue(muschoice,QueueMusic)+QueueMusic;
				//ADD TO QUEUE / ENQUEUE
				system("PAUSE");
			}
			else if(playlistChoice==2)
			{
				//REMOVE FROM QUEUE / DEQUEUE
				QueueMusic=QueueMusic-Music.Dequeue();
				if(QueueMusic<=0)
				{
					QueueMusic=1;
				}
			}
			else if(playlistChoice==3)
			{
				Music.playQueue(QueueMusic);
			}
			else if(playlistChoice==4)
			{
			}
			else
			{
			}
		}
//////////CHOICE 4 / SORT
			else if (choice==4)
		{
			system("CLS");
			Music.sortByAlbum();
			system("PAUSE");
		}
//////////CHOICE 5 / RANDOM PLAYLIST
		else if (choice==5)
		{
			return 0;
		}
	}
} 
