/* Create a C++ program implementing linked lists to store a list of music titles. You must be able to perform the following operations:
- insert a music title
- view a music title, the title previously viewed and the next on queue
- edit a music title
- delete a music title
- view the list of all music titles stored

Sample input: "Buwan by JKL"

Sample output:
Playing - "Everything by Never The Strangers" 
Previous - "Bawat daan by Ebe Dancel"
Next - "Dating Tayo by TJ Monterde" */
	
#include <iostream>
#include <windows.h>
#include <string.h>
#include "QueueLibrary.h"

using namespace std;

void Design(){
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, 3); 
		cout 
		<<"" << endl
		<<"" << endl
		<<"  $     $  $    $ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl
		<<"  $$   $$  $    $ $         $   $             #  #    # ##    #  #       " << endl
		<<"  $ $ $ $  $    $ $         $   $            #   #    # # #   #  #       " << endl
		<<"  $  $  $  $    $ $$$$$$    $   $           #    #    # #  #  #  ####    " << endl
		<<"  $     $  $    $      $    $   $          #     #    # #   # #  #       " << endl
		<<"  $     $  $    $      $    $   $         #      #    # #    ##  #       " << endl
		<<"  $     $  $$$$$$ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl		
		<<"" << endl
		<<"" << endl << endl << endl;
		system("pause");
		system("cls");
	}
void Menu(){
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 6);
	cout
			<< "      " << endl
			<< "                             Main Menu                          " << endl 
			<< "            1 : Insert Music Title                              " << endl
			<< "            2 : View Music Title                                " << endl
			<< "            3 : Edit Music Title                                " << endl 
			<< "            4 : Delete Music Title                              " << endl 
			<< "            5 : View the List of all Music Titles               " << endl 
			<< "            6 : Insert Music (Push)                             " << endl
			<< "            7 : Delete Music (Pop)                              " << endl
			<< "            8 : View Playlist                                   " << endl
			<< "            9 : Add to Playlist                                 " << endl
			<< "           10 : Remove from Playlist                            " << endl
			<< "           11 : Enqueue                                         " << endl		
			<< "           12 : Dequeue                                         " << endl		
			<< "           13 : Display Queue                                   " << endl		
			<< "           14 : Exit                                            " << endl		
			<< "                                                                " << endl 						
			<< "      " << endl 
			<< "              Enter the number of your chosen option: ";	
			
}		
int main()
{
	Design();
		List musicList;
		string musicTitle, editTitle;
		int musicNum;
		int choice;
		int counter = 1;
		Stack playList;
		Queue queueList;
		playList.Push("0");
		playList.Push("Tensionado by Soapdish");
		playList.Push("Ehu Girl by Kolohe Kai");
		playList.Push("Jopay by Mayonnaisse");
		playList.Push("Batang Pasaway by Psychedelic Boyz");
		playList.Push("Leaves by Ben&Ben");
		playList.Push("Maybe The Night by Ben&Ben");
		playList.Push("Hanggang Kailan by Orange&Lemons");
		playList.Push("Mundo by IV of Spades");
		playList.Push("Bulong by December Avenue");
		playList.Push("Kahit Ayaw Mo Na by This Band");	
		musicList.InsertNode(0,"Tensionado by Soapdish");
		musicList.InsertNode(1,"Ehu Girl by Kolohe Kai");
		musicList.InsertNode(2,"Jopay by Mayonnaisse");
		musicList.InsertNode(3,"Batang Pasaway by Psychedelic Boyz");
		musicList.InsertNode(4,"Leaves by Ben&Ben");
		musicList.InsertNode(5,"Maybe The Night by Ben&Ben");
		musicList.InsertNode(6,"Hanggang Kailan by Orange&Lemons");
		musicList.InsertNode(7,"Mundo by IV of Spades");
		musicList.InsertNode(8,"Bulong by December Avenue");
		musicList.InsertNode(9,"Kahit Ayaw Mo Na by This Band");
	int start = 0;
	bool systemLoopCondition = true;
	while(systemLoopCondition == true)
	{
		cout 
		<<"" << endl
		<< "                    Number of Music in Playlist: " << counter-1 << "                       " << endl
		<<"" << endl;
		Menu();	
		cin >> choice;
		while (choice < 1 || choice > 14)
		{
			cout << "Please enter a valid choice" << endl;
			cin >> choice;
			if(cin.fail()){
				cin.clear();
				cin.ignore();
			}
		}
	switch(choice)
	{
		case 1: //Insert
			system("cls");
			cout << "  INSERT MUSIC TITLE " << endl;
			cout << "Example input: Buwan by Juan Karlos Labajo"<<endl;
			cout << "Input the music title you want to Insert: ";
			cin.ignore();
			getline(cin, musicTitle);
			musicList.InsertNode(0,musicTitle);
			musicList.DisplayList();
			system("pause");
			system("cls");
			break;
		case 2://View
			system("cls");
			cout << "  VIEW MUSIC TITLE " << endl;
			cout<<"Enter the number of the music title you want to view: " << endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			if(musicNum <= 1)
				{
				cout<<"Previous: No Song" <<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			else
				{
				cout<<"Previous: "<<musicList.ViewMusic(musicNum-1)<<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			system("pause");
			system("cls");
			break;
		case 3://Edit
			system("cls");
			cout << "  EDIT MUSIC TITLE " << endl;
			cout<<"Enter the number of the music title you want to edit: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			cout<<"Enter the new music title: ";
			cin.ignore();
			getline(cin, editTitle);
			cout<<endl;
			musicList.InsertNode(musicNum-1,editTitle);
			system("pause");
			system("cls");
			break;
		case 4://Delete
			system("cls");
			cout << "  DELETE MUSIC TITLE " << endl;		
			cout<<"Enter the number of the music title you want to delete: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			system("pause");
			system("cls");
			break;
		case 5://View all
			system("cls");
			cout << "  MUSIC LIST " << endl;
			musicList.DisplayPlaylist();
			system("pause");
			system("cls");
			break;
		case 6://Add music using push
			system("cls");
			cout << "  INSERT MUSIC TITLE " << endl;
			cout << "Enter the music title you want to add to Music List: ";
			cin.ignore();
			cin.clear();
			getline(cin, musicTitle);
			playList.Push(musicTitle);
			playList.Display();
			system("pause");
			system("cls");
			break;
		case 7://remove music using pop
			system("cls");
			cout << "  DELETE MUSIC TITLE " << endl;
			cout << " " << endl;
			playList.Pop();
			cout << "  Top Music Title of Music List Removed. " << endl;
			playList.Display();
			system("pause");
			system("cls");
			break;
		case 8://view Playlist
			system("cls");
			cout << "  PLAYLIST " << endl;
			playList.viewPlaylist();
			system("pause");
			system("cls");
			break;
		case 9://add to Playlist
			system("cls");
			cout << "  ADD TO PLAYLIST " << endl;
			cout << " " << endl;
			playList.Display();
			cout << "Enter the music number you want to add to playlist: ";
			cin.ignore();
			cin >> musicNum;
			counter = playList.addMusic(musicNum,counter)+counter;
			system("cls");
			break;
		case 10://remove from Playlist
			system("cls");
			cout << "  REMOVE FROM PLAYLIST " << endl;
			counter = counter-playList.removeMusic(counter);
			cout << " " << endl;
			playList.Display();
			cout << "  Top Music Title of Playlist Removed. " << endl;
			system("pause");
			system("cls");
			break;
		case 11://Enqueue
			system("cls");
			cout << "  ENQUEUE " << endl;
			cout << "Enter the music title you want to add to queueList: ";
			cin.ignore();
			cin.clear();
			getline(cin, musicTitle);
			queueList.Enqueue(musicTitle);
			queueList.displayQueue();
			system("pause");
			system("cls");
			break;
		case 12://Dequeue
			system("cls");
			cout << "  DEQUEUE " << endl;
			cout << " " << endl;
			queueList.Dequeue();
			cout << "  First Music Queue is now Removed. " << endl;
			queueList.displayQueue();
			system("pause");
			system("cls");
			break;
		case 13://Display queue
			system("cls");
			cout << "  ALL QUEUE " << endl;
			queueList.displayQueue();
			system("pause");
			system("cls");
			break;						
		case 14://Exit
			system("cls");
			main();
		default:
			cout<<""<<endl;
			cout<<"     Invalid input! Please try back."<<endl;
			system("pause");
			system("cls");
			break;
	}
}
	while (choice <= 14 && choice >= 1);
	return 0;
	
}
