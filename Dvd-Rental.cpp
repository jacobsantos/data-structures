#include <iostream>
#include <ctime>
#include <windows.h>
#include <string.h>

using namespace std;
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
string movieTitle, dateTime;
int choice, x, y, numMovie;
bool selected;
int options();
string nameCust(string whatName);
int rentDvd(int choice, string firstName, string lastName);

string movieList[25][5] = {
	{"MOMOL Nights","iWant Originals","Benedict Mique","Kit Thompson, Kim Molina","yes"},
	{"Indak","Viva Films","Paul Alexei Basinilio","Sam Concepcion, Nadine Lustre", "yes"},
	{"Just a Stranger",	"Viva Films",	"Jason Paul Laxamana",	"Anne Curtis, Marco Gumabao", "yes"},
	{"The Ghosting",	"Reality Entertainment"	,"Joey de Guzman",	"Andrea Brillantes, Khalil Ramos", "yes"},
	{"Cuddle Weather"	,"Regal Entertainment",	"Rod Marmol"	,"Sue Ramirez, RK Bagatsing", "yes"},
	{"Between Maybes",	"Black Sheep Productions"	,"Jason Paul Laxamana"	,"Julia Barretto, Gerald Anderson", "yes"},
	{"Eerie"	,"Star Cinema",	"Mikhail Red"	,"Bea Alonzo, Jake Cuenca, Maxine Magalona",  "yes"},
	{"Hello, Love, Goodbye"	,"Star Cinema"	,"Cathy Garcia-Molina"	,"Kathryn Bernardo, Alden Richards", "yes"},
	{"Family History",	"GMA Pictures",	"Michael V."	,"Michael V., Dawn Zulueta", "yes"},
	{"Circa",	"Adolf Alix",	"Adolf Alix"	,"Anita Linda, Enchong Dee", "yes"},
	{"The Panti Sisters",	"ALV Films",	"Jun Robles Lana",	"Paolo Ballesteros, Christian Bables, Martin del Rosario",  "yes"},
	{"Clarita"	,"Black Sheep Productions",	"Derick Cabrido",	"Jodi Sta. Maria, Arron Villaflor", "yes"},
	{"Alone/Together"	,"Black Sheep Productions",	"Antoinette Jadaone",	"Liza Soberano, Enrique Gil", "yes"},
	{"Ulan"	,"Viva Films",	"Irene Villamor",	"Carlo Aquino, Nadine Lustre", "yes"},
	{"Kuwaresma",	"Reality Entertainment"	,"Erik Matti"	,"Sharon Cuneta. John Arcilla", "yes"},
	{"Miss Granny",	"N2 Productions",	"Joyce E. Bernal",	"Sarah Geronimo, James Reid, Xian Lim", "yes"},
	{"The Hows of Us",	"Star Cinema"	,"Cathy Garcia-Molina",	"Kathryn Bernardo, Daniel Padilla", "yes"},
	{"My Little Bossings",	"OctoArt Films"	,"Marlon Rivera",	"Vic Sotto, Kris Aquino, Ryzza Mae Dizon, Bimby Yap", "yes"},
	{"Jack Em Popoy: The Puliscredibles",	"APT Entertainment",	"Mike Tuviera",	"Coco Martin, Maine Mendoza, Vic Sotto", "yes"},
	{"Kita Kita",	"Spring Films"	,"Sigrid Andrea Bernardo"	,"Empoy Marquez, Alessandra de Rossi", "yes"},
	{"Heneral Luna",	"Artikulo Uno",	"Jerrold Tarog"	,"John Arcilla, Mon Confiado, Epi Quizon", "yes"},
	{"Ang Panday"	,"CCM Films Productions",	"Rodel Nacianceno",	"Coco Martin", "yes"},
	{"Mary, Marry Me"	,"Ten17P"	,"RC Delos Reyes"	,"Toni Gonzaga, Alex Gonzaga, Sam Milby", "yes"},
	{"Buy Bust",	"Viva Films"	,"Erik Matti",	"Anne Curtis, Brandon Vera", "yes"},
	{"Goyo: The Boy General",	"TBA Studios",	"Jerrold Tarog"	,"Paulo Avelino, Carlo Aquino, Epi Quizon", "yes"}
	};

void storeDesign()
	{
		SetConsoleTextAttribute(hConsole, 91); 
		cout 
		<<"ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl
		<<"ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl
		<<" $$$$$   $       $ $$$$$    ßßßßß  ßßßßß ß     ß ßßßßßßß    ß    ß      ###### ####### ###### ######  ######  " << endl
		<<" $    $  $       $ $    $   ß    ß ß     ßß    ß    ß      ß ß   ß      #    #    #    #    # #     # #       " << endl
		<<" $     $ $       $ $     $  ß    ß ß     ß ß   ß    ß     ß   ß  ß      #         #    #    # #     # #       " << endl
		<<" $     $  $     $  $     $  ßßßßß  ßßß   ß  ß  ß    ß    ß     ß ß      ######    #    #    # #    #  ####    " << endl
		<<" $     $   $   $   $     $  ß  ß   ß     ß   ß ß    ß    ßßßßßßß ß           #    #    #    # #####   #       " << endl
		<<" $    $     $ $    $    $   ß   ß  ß     ß    ßß    ß    ß     ß ß      #    #    #    #    # #   #   #       " << endl
		<<" $$$$$       $     $$$$$    ß    ß ßßßßß ß     ß    ß    ß     ß ßßßßßß ######    #    ###### #    #  ######  " << endl		
		<<"ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl
		<<"ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl << endl << endl;
	}

string nameCust(string whatName)
{
	string name;
	cout << whatName << " Name: ";
	cin >> name;
	return name;
}

int options()
{
	int choice;
	cout << "\n1: Back to Menu \n2: Exit\n>> ";
	cin >> choice;
	system("cls");
	return choice;
}

int main()
{
	storeDesign();
	SetConsoleTextAttribute(hConsole, 6);
	cout << "--------------------------------   Welcome to DVD RENTAL STORE   --------------------------------\n" << endl;
	cout << "1) Main Menu " << endl;
	cout << "2) Exit " << endl;
	cout << "  >>>>  ";
	cin >> choice;
	system("cls");
	cout << "ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl;
	cout << "Please enter your details." << endl;
	string firstName = nameCust("First");
	string lastName = nameCust("Last");
	time_t now = time(0);
    char* dt = ctime(&now);
    dateTime = dt;
    cout << "The date and time of your transaction is: " << dt << endl;
	cout << "ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl;
	system("pause");
	system("cls");
	while(choice == 1){	
			cout
			<< "ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl
			<< "ß                       Main Menu                          ß" << endl 
			<< "ß      1 : Rent a DVD                                      ß" << endl
			<< "ß      2 : Return a DVD                                    ß" << endl
			<< "ß      3 : Look for a DVD                                  ß" << endl 
			<< "ß      4 : List of all DVDs                                ß" << endl 
			<< "ß      5 : Pay Counter                                     ß" << endl 
			<< "ß      6 : Exit                                            ß" << endl
			<< "ß                                                          ß" << endl  
			<< "ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl << endl
			<< "Enter the number of your chosen option: ";
		cin >> choice;
		system("cls");
		choice = rentDvd(choice, firstName, lastName);
	}
	if (choice == 0)
	{
		int cash=0, total = numMovie * 35;
		char yes;
		cout << "Welcome to Payment Area." << endl;
		cout << "You rented " << numMovie << " movie/s" << endl;
		cout << "Proceed to Payment? (y/n)" << endl;
		cin >> yes;
		if (( yes == 'y')|| (yes == 'Y'))
		{
		cout << "Here is the total price of the Movie/s you rented: P" << total << endl;
		cout<<"Cash Tendered: ";
		cin >> cash;
		cout << "Change: P" << cash-total << endl;
		cout << "Thank you!";
		system("pause");
		system("cls");
		return main();
		}
		else
		{
		cout << "Are you sure? (y/n)" << endl;
		cin >> yes;	
		if (( yes == 'y')|| (yes =='Y'))
		 { 
			cout << "Thank you for your service, please come again! Have a safe trip!";
			system("pause");
			system("cls");
			return main();
			
		 }
		 else{
		 	system("cls");
		 	return main();
		 	
		 }
		}		
	}
	cout << "Thank you! Please come again!" << endl;
	return 0;
}

int rentDvd(int choice, string firstName, string lastName){
	x = 0;
	if (choice == 1){
		cout << "MOVIE TITLES" << endl;
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ") " << movieList[i][0];
			if (movieList[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Not Available)\n";
			}
		}
		cout << "\nTitle of the movie: ";
		cin.get();
		getline(cin, movieTitle);
		for (int i = 0; i <= 24; i++){
			if(movieList[i][0] == movieTitle){
				movieList[i][4] = "no";
				selected = true;
				x = i;
			}
		}
		if (selected == 1){
			cout << "The movie " << movieList[x][0];
			if(movieList[x][4] == "no" && y == 1){
			cout << " is unavailable.\n";
			}
			else
				{
				cout << " has been rented.\n";
				y = 0;
				}
			}
		
		else{
			cout << "The movie is not on the list.\n";
		}
		numMovie++;
		x = 2;
		selected = false;
	}
	else if (choice == 2){
		cout << "Movie list\n";
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ") " << movieList[i][0];
			if (movieList[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Not Available)\n";

			}
		}
		cout << "\nWhat is the Title of the movie you'll return? ";
		cin.get();
		getline(cin, movieTitle);
		for (int i = 0; i <= 24; i++){
			if(movieList[i][0] == movieTitle){
				movieList[i][4] = "yes";
				selected = true;
				x = i;

			}
		}
		if (selected == 1){
			cout << "The movie " << movieList[x][0];
			if(movieList[x][4] == "yes" && y == 1){
			cout << " is available.\n";
			
			}
			else{
			cout << " is now available.\n";
			y = 0;
			numMovie -=1;
			//system("pause")
			}
		}
		else{
			cout << "The movie is not on the list.\n";
		}
		x = 2;
		selected = false;
		//system("pause");
	}
	else if (choice == 3){
		cout << "Search: ";
		cin.get();
		getline(cin, movieTitle);
		for (int i = 0; i <= 24; i++){
			if(movieList[i][0] == movieTitle){
				cout << "\nMovie Title: " << movieList[i][0] << endl;
				cout << "Producer: " << movieList[i][1] << endl;
				cout << "Director: " << movieList[i][2] << endl;
				cout << "Starring: " << movieList[i][3] << endl;
				cout << "Status:  ";
				if (movieList[i][4] == "yes"){
					cout << "Available\n";
				}
				else{
					cout << "Not Available\n";
				}
				selected = true;
			}
		}
		if(selected != 1){
				cout << "Sorry, that movie is unavailable.\n";
			}
		selected = false;
		//system("pause");
	}
	else if (choice == 4){
		cout << "Movie list\n";
		for (int i = 0; i <= 24; i++){
			cout << i + 1 << ". " << movieList[i][0];
			if (movieList[i][4] == "yes"){
				cout << endl;
			}
			else{
				cout << "(Not Available)\n";
			//	system("pause");
			}
		}
	}
	else if (choice == 5){
		cout << "Name: " << firstName << " " << lastName << endl;
		cout << "Date of Transaction: " << dateTime << endl;
		if (y == 0){
			cout << "\nMovies Rented: "  << numMovie << endl;
			for (int i = 0; i <= 24; i ++){
				if (movieList[i][4] == "no"){
					cout << movieList[i][0] << endl;
				//	system("pause");
				}
			}
		}
		else{
				cout << "\nYou haven't chosen a DVD to rent. Try Again." << endl;
			//	system("pause");
		}
		cout << "\n0: Pay Out";
	}
	else if (choice == 6){
		return 2;
		system("pause");
	}
	return options();
}
