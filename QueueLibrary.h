#include <iostream>
#include <string.h>
#include <string>

using namespace std;
///////////// LINK LIST LIBRARY
class Node{
	public:
		string data;
		Node * next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			Node* InsertPlaylist(int index,string x);
			void DisplayList(void);
			void DisplayPlaylist(void);
			string ViewMusic(int index);
	private:
			Node* head;
};
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
void List::DisplayPlaylist(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::InsertNode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
Node* List:: InsertPlaylist(int index, string x){
if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
////////////////////// STACKS LIBRARY
class Stack{
	public:
		Nullifier();
		void Push(string Title);
		void Pop();
		void Display();
		void viewPlaylist();// in view menu
			int addMusic(int add, int count); //add to playlist
			int removeMusic(int count);  // remove from playlist
	private:
		struct Node // struct
		{
			Node* next;
			int data, Playlist; 
			string Title;
			bool checker;	
		} typedef *pointer; // set as pointer datatype
		pointer head;
		pointer curr;
		pointer top;
		pointer temp;
};

Stack::Nullifier()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}


void Stack::Push(string Title) //push stacks
{
	pointer link = new Node; // creating new node
	link->next = NULL; //no data
		if(head!=NULL) //empty head
			{
			curr = head;
			temp = head;
			while(curr->next != NULL) 
			{
				curr = curr->next;
				temp = curr;
			}
			curr->next = link;
			curr = curr->next;
			curr->data = temp->data+1; // "pushing" node / adding to stacks
			}
		else
		{
			head = link; //make the new node the head
			link->data = 0; //back to null
		}
	top = link; //top of stack
	link->Title = Title; //to title
	link->checker = false; //to check
	link->Playlist = 0; //playlist
}

void Stack::Pop() //delete stacks
{
	curr=head; 
	temp=head;
//	int newTopData;
	if(curr->next==NULL||curr==NULL) //check if empty stack
	{
		cout<<"Your Playlist is empty."<<endl;
	}
	else
	{
		while(curr->data!=top->data-1) //loop for checking the num of stacks
		{
			temp=curr;
			curr=curr->next;
		}
		if(curr->data==0) //empty 
		{
			curr->next=NULL;
			top=curr;
			cout<<"No more music in the Playlist."<<endl;	
		}
		else //remover
		{
			top=curr;
			curr->next = NULL;
		}	
	}
}

void Stack::Display() //display list of songs
{
	cout << " 様様様様様様様様様様 Music List 様様様様様様様様様様" << endl;
	for(int i = top->data ;i>=0 ;i--)	
	{
		curr=head;
		temp=head;
		while(curr!=NULL &&curr->data!=i) //stack is not empty
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0)
		{
		}
		else
		{
			cout << curr->data << ") " << curr->Title << endl;
		}
	} 
	cout << "様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�" << endl;
	cout << "Number of Songs: " << top->data <<endl;
	cout << "様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�" << endl;
}

void Stack::viewPlaylist() //display playlist
{
	for(int i = top->data;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->Playlist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0||curr->Playlist==0||curr->checker==false)
		{
		}
		else
		{
			cout << curr->Playlist << ") " << curr->Title << endl;
		}
	}
}

int Stack::addMusic(int add, int count) //add to playlist
{
	pointer answer = NULL;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != add) 
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data == 0) // if number not on the list
	{
		cout << "The music number " << add << " is not on the playlist." << endl;
		delete answer;
		return 0;
	}
	else if(curr->checker==true) // delete if true
	{
		return 0;
	}
	else
	{	
		curr->Playlist = count;
		curr->checker = true;
		return 1; 
	}
}

int Stack::removeMusic(int count)
{
	int delMusic = count -1;
		pointer answer = NULL; 
		curr=head;
		temp=head;
		while(curr != NULL && curr-> Playlist != delMusic)
		{
			temp = curr;
			curr = curr->next;
		}
		if (curr == NULL or curr->data ==0)
		{
			delete answer;
			return 0;
		}
		else if(curr->checker==false)
		{
			return 0;
		}
		else
		{	
			curr->Playlist = 0;
			curr->checker =false;
			return 1; 
		}
}
////////////QUEUE LIBRARY////////////////
class Queue{
	public:
		Nullifier();
		void Enqueue(string enq);
		void Dequeue();
		void displayQueue();
	private:
		struct Node // struct
		{
			Node* next;
			int data, Playlist; 
			string enq;
			bool checker;
		} typedef *pointer; // set as pointer datatype
		pointer head;
		pointer curr;
		pointer back;
		pointer temp;
};

void Queue::Enqueue(string enq) // add to the back of the node
{
	pointer En = new Node;
	En->next = NULL;
	if(head!=NULL)
	{
		curr = head;
		temp= head;
		while(curr->next != NULL)
		{
			temp = curr;
			curr = curr->next;
		}
		curr->next = En;
		back = En;
		En->data = curr->data+1;
	}
	else
	{
		head = En;
		back = En;
		En->data = 1;
	}
	En -> enq = enq;
	En -> checker = false;
	En -> Playlist = 0;
}

void Queue::Dequeue() // removes the front of the node
{
	if(head == NULL)
	{
		cout << "No Queue " <<endl;
	}
	else if(head->next==NULL)
	{
		
	}
	curr = head;
	temp = curr;
	head = curr->next;
	curr = curr->next;
	temp = NULL;
	while(curr->next!=NULL)
	{
		curr->data=curr->data-1;
		curr=curr->next;
	}
}

void Queue::displayQueue() // show queue
{
	curr = head;
	while(curr!=NULL)
	{
		if(curr->data <=0)
		{
			curr = curr->next;
		}
		else
		{
			cout << curr->data <<") " << curr-> enq <<endl;
			curr = curr->next;
		}
	}
}

