/* Create a C++ program implementing linked lists to store a list of music titles. You must be able to perform the following operations:
- insert a music title
- view a music title, the title previously viewed and the next on queue
- edit a music title
- delete a music title
- view the list of all music titles stored

Sample input: "Buwan by JKL"

Sample output:
Playing - "Everything by Never The Strangers" 
Previous - "Bawat daan by Ebe Dancel"
Next - "Dating Tayo by TJ Monterde" */
	
#include <iostream>
#include <windows.h>
#include <string.h>
#include "NodeLibrary.h"

using namespace std;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
int choice;
void Design(){
		SetConsoleTextAttribute(hConsole, 3); 
		cout 
		<<"ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl
		<<"ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl
		<<"  $     $  $    $ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl
		<<"  $$   $$  $    $ $         $   $             #  #    # ##    #  #       " << endl
		<<"  $ $ $ $  $    $ $         $   $            #   #    # # #   #  #       " << endl
		<<"  $  $  $  $    $ $$$$$$    $   $           #    #    # #  #  #  ####    " << endl
		<<"  $     $  $    $      $    $   $          #     #    # #   # #  #       " << endl
		<<"  $     $  $    $      $    $   $         #      #    # #    ##  #       " << endl
		<<"  $     $  $$$$$$ $$$$$$  $$$$$ $$$$$$    ###### ###### #     #  ######  " << endl		
		<<"ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl
		<<"ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl << endl << endl;
		system("pause");
		system("cls");
	}
void Menu(){
	SetConsoleTextAttribute(hConsole, 6);
	cout
			<< "      ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl
			<< "      ß                       Main Menu                          ß" << endl 
			<< "      ß      1 : Insert Music Title                              ß" << endl
			<< "      ß      2 : View Music Title                                ß" << endl
			<< "      ß      3 : Edit Music Title                                ß" << endl 
			<< "      ß      4 : Delete Music Title                              ß" << endl 
			<< "      ß      5 : View the List of all Music Titles               ß" << endl 
			<< "      ß      6 : Add to Playlist                                 ß" << endl
			<< "      ß      7 : View Playlist                                   ß" << endl
			<< "      ß      8 : Exit                                            ß" << endl
			<< "      ß                                                          ß" << endl  
			<< "      ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß" << endl 
			<< "              Enter the number of your chosen option: ";	
			cin >> choice;
}		
int main()
{
	Design();
		List musicList, playList;
		string musicTitle, editTitle;
		int musicNum;
		musicList.InsertNode(0,"Tensionado by Soapdish");
		musicList.InsertNode(1,"Ehu Girl by Kolohe Kai");
		musicList.InsertNode(2,"Jopay by Mayonnaisse");
		musicList.InsertNode(3,"Batang Pasaway by Psychedelic Boyz");
		musicList.InsertNode(4,"Leaves by Ben&Ben");
		musicList.InsertNode(5,"Maybe The Night by Ben&Ben");
		musicList.InsertNode(6,"Hanggang Kailan by Orange&Lemons");
		musicList.InsertNode(7,"Mundo by IV of Spades");
		musicList.InsertNode(8,"Bulong by December Avenue");
		musicList.InsertNode(9,"Kahit Ayaw Mo Na by This Band");
		back:
	Menu();
	switch(choice)
	{
		case 1: //Insert
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ INSERT MUSIC TITLE ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			cout << "Example input: Buwan by Juan Karlos Labajo"<<endl;
			cout << "Input the music title you want to Insert: ";
			cin.ignore();
			getline(cin, musicTitle);
			musicList.InsertNode(0,musicTitle);
			musicList.DisplayList();
			system("pause");
			system("cls");
			goto back;
		case 2://View
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ VIEW MUSIC TITLE ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			cout<<"Enter the number of the music title you want to view: " << endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			if(musicNum <= 1)
				{
				cout<<"Previous: No Song" <<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			else
				{
				cout<<"Previous: "<<musicList.ViewMusic(musicNum-1)<<endl;
				cout<<"Playing: "<<musicList.ViewMusic(musicNum)<<endl;
				cout<<"Next: "<<musicList.ViewMusic(musicNum+1)<<endl;
				}
			system("pause");
			system("cls");
			goto back;
		case 3://Edit
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ EDIT MUSIC TITLE ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			cout<<"Enter the number of the music title you want to edit: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			cout<<"Enter the new music title: ";
			cin.ignore();
			getline(cin, editTitle);
			cout<<endl;
			musicList.InsertNode(musicNum-1,editTitle);
			system("pause");
			system("cls");
			goto back;
		case 4://Delete
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ DELETE MUSIC TITLE ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;		
			cout<<"Enter the number of the music title you want to delete: "<< endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			musicList.DeleteNode(musicNum);
			system("pause");
			system("cls");
			goto back;
		case 5://View all
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ MUSIC LIST ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			musicList.DisplayPlaylist();
			system("pause");
			system("cls");
			goto back;
		case 6://Add to Playlist
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ ADD TO PLAYLIST ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			cout << "Enter the number of the music title you want to add to playlist: " << endl;
			musicList.DisplayList();
			cin.ignore();
			cin >> musicNum;
			playList.InsertPlaylist(0, musicList.ViewMusic(musicNum));
			playList.DisplayList();
			system("pause");
			system("cls");
			goto back;
		case 7://view Playlist
			system("cls");
			cout << " ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ PLAYLIST ÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ" << endl;
			playList.DisplayPlaylist();
			system("pause");
			system("cls");
			goto back;
		case 8://Exit
			system("cls");
			main();
		default:
			cout<<""<<endl;
			cout<<"     Invalid input! Please try back."<<endl;
			system("pause");
			system("cls");
			goto back;
	}	
}